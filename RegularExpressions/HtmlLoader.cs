﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using System.IO;

namespace RegularExpressions
{
    class HtmlLoader
    {

        public string HtmlString { get; set; }
        string _url;
        public  HtmlLoader(string url)
        {
            _url = url;           
            using ( HttpClient client = new HttpClient())
            {
                var response = client.GetAsync(url);
                HtmlString = response.Result.Content.ReadAsStringAsync().Result;
            }
        }

        public void WriteImage(string ImageURL)
        {
            using (HttpClient client = new HttpClient())
            {
                string fullpath = ImageURL;
                string[] splittedURL = _url.Split('/');
                string baseURL = splittedURL[0] + "//" + splittedURL[2];
                string path = "";
                if (fullpath.StartsWith("http"))
                    path = fullpath;
                else
                {
                    path = baseURL + fullpath;
                }
                var resp = client.GetByteArrayAsync(path);
                if ((resp.Result != null))
                {
                    string[] s = ImageURL.Split('/');
                    File.WriteAllBytes(Environment.CurrentDirectory + "\\" + s[s.Length - 1], resp.Result);
                }
            }
        }
    }
}
