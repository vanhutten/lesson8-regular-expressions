﻿using System;
using System.Text.RegularExpressions;

namespace RegularExpressions
{
    class Program
    {
        static void Main(string[] args)
        {
            string url;
            Console.WriteLine("Type URL to parse");
            url = Console.ReadLine();
            HtmlLoader loader = new HtmlLoader(url);
            string expression = "(?<=<img.+?)(?<=src=\")(.+?\\.(jpg|gif|png|jpeg))";
            RegexOptions options = RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Multiline;
            Regex rg = new Regex(expression, options);
            foreach (Match match in rg.Matches(loader.HtmlString))
            {
                Console.WriteLine(match.Value);
                loader.WriteImage(match.Value);
            }
            Console.WriteLine("Парсинг закончился. Press Enter");
            Console.ReadLine();

        }
    }
}
